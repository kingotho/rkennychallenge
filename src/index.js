import { Scene, 
	PerspectiveCamera, 
	WebGLRenderer, 
	WebGLObjects, 
	WebGLAttributes, 
	AmbientLight, 
	BufferGeometry, 
	Vector3, 
	Matrix4, 
	VertexColors, 
	ShaderMaterial, 
	Mesh, 
	Color, 
	Math, 
	BufferAttribute, 
	Float32BufferAttribute, 
	Int16BufferAttribute,
	Uint8BufferAttribute
	 } from 'three/build/three.module';

// scene setup
const scene = new Scene();
// const camera = new PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
const camera = new PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 1, 8000 );
const renderer = new WebGLRenderer({ antialias: true, clearColor: 0x050505, clearAlpha: 1, alpha: false } );
// const renderer = new WebGLRenderer({ antialias: false, clearColor: 0x050505, clearAlpha: 1, alpha: false } );
const light = new AmbientLight(0x444444);
camera.position.z = 5;

scene.add(light);
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );


var triangles = 12 * 150000;

// BufferGeometry with unindexed triangles
// use vertex colors to store centers of rotations

var geometry = new BufferGeometry();

var positions = [];
var normals = [];
var colors = [];

// Generate a single buffer with all the cubes

var n = 8000, n2 = n/2;	// triangles spread in the cube
var d = 10, d2 = d/2;	// individual triangle size


var color = new Color();

var pA = new Vector3();
var pB = new Vector3();
var pC = new Vector3();

var cb = new Vector3();
var ab = new Vector3();

var m = new Matrix4();
var m2 = new Matrix4();

var e = new Vector3( 0, 0, 0 );
var t = new Vector3();
var tt = new Vector3();
var u = new Vector3( 0, 1, 0 );

var v1 = new Vector3( 0, 0, 0 );
var v2 = new Vector3( d, 0, 0 );
var v3 = new Vector3( d, d, 0 );
var v4 = new Vector3( 0, d, 0 );

var v1b = new Vector3( 0, 0, d );
var v2b = new Vector3( d, 0, d );
var v3b = new Vector3( d, d, d );
var v4b = new Vector3( 0, d, d );


function addTriangle( k, x, y, z, vc, vb, va ) {

	// positions
	pA.copy( va );
	pB.copy( vb );
	pC.copy( vc );

	t.set( x, y, z );

	m2.makeTranslation( - d2,  - d2,  - d2 );
	pA.applyMatrix4(m2);
	pB.applyMatrix4(m2);
	pC.applyMatrix4(m2);


	t.multiplyScalar( 0.5 );


	m.lookAt( e, tt, u );

	m2.makeTranslation( t.x , t.y , t.z  );

	m2.multiply( m );

	pA.applyMatrix4(m2);
	pB.applyMatrix4(m2);
	pC.applyMatrix4(m2);

	var ax = pA.x;
	var ay = pA.y;
	var az = pA.z;

	var bx = pB.x;
	var by = pB.y;
	var bz = pB.z;

	var cx = pC.x;
	var cy = pC.y;
	var cz = pC.z;

	positions.push( ax, ay, az );
	positions.push( bx, by, bz );
	positions.push( cx, cy, cz );

	// flat face normals
	pA.set( ax, ay, az );
	pB.set( bx, by, bz );
	pC.set( cx, cy, cz );

	cb.subVectors( pC, pB );
	ab.subVectors( pA, pB );

	cb.cross( ab );

	cb.normalize();

	var nx = cb.x;
	var ny = cb.y;
	var nz = cb.z;

	normals.push( nx * 32767, ny * 32767, nz * 32767 );
	normals.push( nx * 32767, ny * 32767, nz * 32767 );
	normals.push( nx * 32767, ny * 32767, nz * 32767 );

	// colors
	color.setRGB( t.x, t.y, t.z );

	colors.push( color.r, color.g, color.b );
	colors.push( color.r, color.g, color.b );
	colors.push( color.r, color.g, color.b );

}

//
var c = 0;
for ( var i = 0; i < triangles; i += 12 ) {

	var x = (window.Math.random() * n) - n/2;
	var y = (window.Math.random() * n) - n/2;
	var z = (window.Math.random() * n) - n/2;

	tt.set( window.Math.random(), window.Math.random(), window.Math.random() );

	addTriangle( c, x, y, z, v1, v2, v4 );
	addTriangle( c, x, y, z, v2, v3, v4 );

	addTriangle( c, x, y, z, v4b, v2b, v1b );
	addTriangle( c, x, y, z, v4b, v3b, v2b );

	addTriangle( c, x, y, z, v1b, v2, v1 );
	addTriangle( c, x, y, z, v1b, v2b, v2 );

	addTriangle( c, x, y, z, v2b, v3, v2 );
	addTriangle( c, x, y, z, v2b, v3b, v3 );

	addTriangle( c, x, y, z, v3b, v4, v3 );
	addTriangle( c, x, y, z, v3b, v4b, v4 );

	addTriangle( c, x, y, z, v1, v4, v1b );
	addTriangle( c, x, y, z, v4, v4b, v1b );
	c++;

}

var positionAttribute = new Float32BufferAttribute( positions, 3 );
var normalAttribute = new Int16BufferAttribute( normals, 3 );
var colorAttribute = new Float32BufferAttribute( colors, 3 );

normalAttribute.normalized = true; // this will map the buffer values to 0.0f - +1.0f in the shader

geometry.addAttribute( 'position', positionAttribute );
geometry.addAttribute( 'normal', normalAttribute );
geometry.addAttribute( 'color', colorAttribute );

geometry.computeBoundingSphere();

// Set up custom shader material

var uniforms = {
	amplitude: { type: "f", value: 20.0 },
	userColor: { value: new Color( 0xff0000 ) }
};

var material = new ShaderMaterial( {
	uniforms: 		uniforms,
	vertexShader:   document.getElementById( 'vertexShader' ).textContent,
	fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
	vertexColors:   VertexColors
});

var mesh = new Mesh( geometry, material );
scene.add( mesh );

mesh.position.z = - n/3;

var startTime = Date.now();

// render loop
const update = () =>{
  	requestAnimationFrame(update);

	var secs = window.Math.floor((Date.now() - startTime)/1000);
	//Reveal a new cube every second
	geometry.setDrawRange(0, secs * 12 * 3);

  	renderer.render(scene, camera);
}

//UI update methods
window.changeBoxScale = function changeBoxScale(s) {
	uniforms.amplitude.value = s;
}

window.changeBoxColor = function changeBoxColor(c) {
	switch (c) {
		case "red" :
			uniforms.userColor.value = new Color( 0xff0000 );
			break;
		case "yellow" :
			uniforms.userColor.value = new Color( 0xffff00 );
			break;
		case "green" :
			uniforms.userColor.value = new Color( 0x00ff00 );
			break;
		case "cyan" :
			uniforms.userColor.value = new Color( 0x00ffff );
			break;
		case "blue" :
			uniforms.userColor.value = new Color( 0x0000ff );
			break;
		case "magenta" :
			uniforms.userColor.value = new Color( 0xff00ff );
			break;
	}
}


// function calls
update();
