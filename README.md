### Notes ###

Since an unknown number of cubes were to be rendered I wanted to take a very performant approach. I opted for generating a single BufferGeometry object with 150,000 cubes in random positions and orientations. I store the individual cube center points as vertex colors, and do the cube scaling in the vertex shader. The cubes are revealed one every second utilizing the BufferGeometry setDrawRange() function. Color is applied in the fragment shader.

For this project I drew inspiration from these examples:

[Altered Qualia - WebGl Cubes](http://alteredqualia.com/three/examples/webgl_cubes.html)

[Mr. Doob - Three.js BufferGeometry Example](https://threejs.org/examples/#webgl_buffergeometry_uint)


Roger Kenny
roger@rogerkenny.com
twitter @RogerWallstreet